# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 's%a9w==#ecj_-4-09b27qz%%zz1u$yyz@jmumyly6we2=cu!08'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}